import React, { useState } from 'react';
import got from './data_2';

import './App.css';

import ProfileCard from './Components/ProfileCard';
import Button from './Components/Button';

function App() {
  let [dataUI,setData] = useState(got.houses)
  // let [searchUI,setSearchUI] = useState('')
  function buttonHandler (event) {
    let familyName = event.target.innerText;
    setData(got.houses.filter((family)=> family.name === familyName))
  }

  function search (event) {
    let familyName = event.target.value;
    let tempo = JSON.stringify(got.houses);
    let tempData = JSON.parse(tempo);
    if (familyName === '') {
      setData(got.houses)
    }
    else {
      for (let index=0; index<tempData.length; index++) {
        tempData[index]['people']= tempData[index]['people'].filter((persons) => persons['name'].toLowerCase().includes(familyName.toLowerCase()))
      }
      setData(tempData);
    }

  } 
  return (
    <div>
       <div className='top-section'>
        <h1>People of GOT 👑</h1>
        <input type='search' placeholder="Search the people of GOT" onKeyUp= {search}/>
       </div>
       <Button houses={got.houses} listener = {buttonHandler}></Button>
       <ProfileCard  className = 'profile-container' houses = {dataUI}></ProfileCard>
  
    </div>
  )
}

export default App;
