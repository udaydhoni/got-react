import React from "react";

function ProfileCard (props) {
    return (
      <div className= 'profile-container'>
        {props.houses.map((family) => (
            family['people'].map((member)=>{
                // console.log(member)
                return(
                    <div className="profile">
                     <img src={member.image} />
                     <h2>{member['name']}</h2>
                     <p>{member['description']}</p>
                     <button>KNOW MORE</button>
                 </div>
                )
                
            })
        )
        )}
      </div>   
    )
}

export default ProfileCard