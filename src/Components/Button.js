
function Button (props) {
    console.log(props);
    return (
        <div className="buttons">
            {
                props.houses.map((family)=>(
                    <button onClick={props.listener}>{family['name']}</button>
                ))
            }
        </div>
        
    ) 
}

export default Button